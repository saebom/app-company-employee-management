import 'package:app_company_employee_manager/middleware/middleware_login_check.dart';
import 'package:flutter/material.dart';

class LoginCheck extends StatefulWidget {
  const LoginCheck({Key? key}) : super(key: key);

  @override
  State<LoginCheck> createState() => _LoginCheckState();
}

class _LoginCheckState extends State<LoginCheck> {
  @override
  void initState() {
    super.initState();
    MiddleWareLoginCheck().check(context);
  }
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
