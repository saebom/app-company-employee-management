import 'package:app_company_employee_manager/model/employee/employee_login_response.dart';

class EmployeeLoginSingleResult {
  bool isSuccess;
  int code;
  String msg;
  EmployeeLoginResponse data;

  EmployeeLoginSingleResult(this.isSuccess, this.code, this.msg, this.data);

  factory EmployeeLoginSingleResult.fromJson(Map<String, dynamic> json) {
    return EmployeeLoginSingleResult(
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
      EmployeeLoginResponse.fromJson(json['data'])
    );
  }


}