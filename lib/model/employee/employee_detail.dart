class EmployeeDetail {
  int employeeId;
  String employeeName;
  int employeeNumber;
  String username;
  String employeeBirthday;
  String employeeField;
  String employeeGrade;
  String dateIn;

  EmployeeDetail(this.employeeId, this.employeeName, this.employeeNumber, this.username, this.employeeBirthday, this.employeeField, this.employeeGrade, this.dateIn);

  factory EmployeeDetail.fromJson(Map<String, dynamic> json) {
    return EmployeeDetail(
      json['id'],
      json['employeeName'],
      json['employeeNumber'],
      json['username'],
      json['employeeBirthday'],
      json['employeeField'],
      json['employeeGrade'],
      json['dateIn'],
    );
  }
}