import 'package:app_company_employee_manager/model/employee/employee_detail.dart';

class EmployeeDetailResult {
  bool isSuccess;
  int code;
  String msg;
  EmployeeDetail data;

  EmployeeDetailResult(this.isSuccess, this.code, this.msg, this.data);

  factory EmployeeDetailResult.fromJson(Map<String, dynamic> json) {
    return EmployeeDetailResult(
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
      EmployeeDetail.fromJson(json['data']),
    );
  }
}