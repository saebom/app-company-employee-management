class VacationCreateRequest {
  String vacationName;
  String vacationType;
  String dateVacation;
  String vacationReason;

  VacationCreateRequest(this.vacationName, this.vacationType, this.dateVacation, this.vacationReason);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['vacationName'] = this.vacationName;
    data['vacationType'] = this.vacationType;
    data['dateVacation'] = this.dateVacation;
    data['vacationReason'] = this.vacationReason;

    return data;
  }
}
