import 'package:app_company_employee_manager/model/attendance/attendance_situation_response.dart';

class AttendanceSituationSingleResult {
  bool isSuccess;
  int code;
  String msg;
  AttendanceSituationResponse data;

  AttendanceSituationSingleResult(this.isSuccess, this.code, this.msg, this.data);

  factory AttendanceSituationSingleResult.fromJson(Map<String, dynamic> json) {
    return AttendanceSituationSingleResult(
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
      AttendanceSituationResponse.fromJson(json['data']),
    );
  }
}