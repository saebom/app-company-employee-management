import 'package:app_company_employee_manager/components/component_appbar_popup.dart';
import 'package:app_company_employee_manager/components/component_attendance_list.dart';
import 'package:app_company_employee_manager/model/vacation/vacation_list_item.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:timer_builder/timer_builder.dart';

class PageAttendanceCheck extends StatefulWidget {
  const PageAttendanceCheck({Key? key}) : super(key: key);

  @override
  State<PageAttendanceCheck> createState() => _PageAttendanceCheckState();
}

class _PageAttendanceCheckState extends State<PageAttendanceCheck> {
  var now = DateTime.now();

  final _scrollController = ScrollController();

  List<VacationListItem> _list = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(
        title: '출퇴근 조회',
      ),
      body: ListView(
        controller: _scrollController,
        children: [
          _buildBody()
        ],
      ),
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(height: 10,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              OutlinedButton(
                  onPressed: () {},
                  child: const Text('< 이전달')
              ),
              TimerBuilder.periodic(
                Duration(seconds: 1),
                builder: (context) {
                  return Text(
                    '${DateFormat('yyyy-MM').format(DateTime.now())}',
                    style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                  );
                },
              ),
              OutlinedButton(
                  onPressed: () {},
                  child: const Text('다음달 >')
              )
            ],
          ),
          SizedBox(height: 10,),
          Container(
            child: Column(
              children: [
                DataTable(
                  decoration:BoxDecoration(
                    border: Border.all(color: Colors.black)
                  ) ,
                  columns: [
                      DataColumn(label: Text('출근', style: TextStyle(fontWeight: FontWeight.bold,),)),
                      DataColumn(label: Text('지각', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.orange),)),
                      DataColumn(label: Text('반차', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.blue),)),
                      DataColumn(label: Text('연차', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.indigo),)),
                      DataColumn(label: Text('결근', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.red),)),
                    ],
                  rows: [
                      DataRow(
                          cells: [
                            DataCell(Text('1')),
                            DataCell(Text('1', style: TextStyle(color: Colors.orange),)),
                            DataCell(Text('1', style: TextStyle(color: Colors.blue),)),
                            DataCell(Text('1', style: TextStyle(color: Colors.indigo),)),
                            DataCell(Text('1', style: TextStyle(color: Colors.red),)),
                          ]
                      )
                    ]
                ),
                SizedBox(height:30,),
                Column(
                  children: [
                    Container(
                      height: 50,
                      decoration: BoxDecoration(border: Border.all(color: Colors.black),),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Text('날짜'),
                          Text('출근시간'),
                          Text('퇴근시간'),
                          Text('상태'),
                        ],
                      ),
                    ),
                    Divider(),
                    ListView.builder(
                        padding:const EdgeInsets.all(8),
                        physics: const NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: _list.length,
                        itemBuilder:(_ , index) => ComponentAttendanceList(item: _list[index])
                    )
                  ],
                ),
              ],
            ),
          )

        ],
      ),
    );
  }
}
