import 'package:app_company_employee_manager/components/component_appbar_popup.dart';
import 'package:app_company_employee_manager/components/component_custom_loading.dart';
import 'package:app_company_employee_manager/components/component_notification.dart';
import 'package:app_company_employee_manager/repository/repo_attendance.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:timer_builder/timer_builder.dart';

class PageAttendanceState extends StatefulWidget {
  const PageAttendanceState({Key? key}) : super(key: key);

  @override
  State<PageAttendanceState> createState() => _PageAttendanceState();
}

class _PageAttendanceState extends State<PageAttendanceState> {
  var now = DateTime.now();
  String _attendanceSituation = '';
  String _timeCompanyIn = '-';
  String _timeCompanyOut = '-';
  String _timeEarlyLeave = '-';

  void initState() {
    super.initState();
    _getMyAttendance();
  }

  Future<void> _getMyAttendance() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoAttendance().getMySituation().then((res) {
      BotToast.closeAllLoading();
      setState(() {
        _attendanceSituation = res.data.attendanceSituation;
        _timeCompanyIn = res.data.timeCompanyIn;
        _timeCompanyOut = res.data.timeCompanyOut;
        _timeEarlyLeave = res.data.timeEarlyLeave;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();
    });
  }

  Future<void> _setAttendance() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoAttendance().setAttendance().then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '출근 등록 성공',
        subTitle: '출근이 정상적으로 등록되었습니다.',
      ).call();

      setState(() {
        if (res.isSuccess == true) {
          _getMyAttendance();
        }
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '출근 등록 실패',
        subTitle: '출근 등록이 실패하였습니다. 다시 시도해주세요.',
      ).call();
    });
  }

  Future<void> _putAttendance(String attendanceSituation) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoAttendance()
        .putAttendanceSituation(attendanceSituation)
        .then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '근태 수정 성공',
        subTitle: '근태 상황이 성공적으로 수정되었습니다.',
      ).call();

      setState(() {
        _getMyAttendance();
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '근태 수정 실패',
        subTitle: '근태 상황 수정이 실패하였습니다. 다시 시도해주세요.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: ComponentAppbarPopup(title: '출 퇴근 등록'), body: _buildBody());
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(
            height: 50,
          ),
          TimerBuilder.periodic(
            Duration(seconds: 1),
            builder: (context) {
              return Text(
                '${DateFormat('yyyy년 MM월 dd일\n    hh:mm:ss a').format(DateTime.now())}',
                style: TextStyle(
                  fontSize: 30,
                  color: Colors.black54
                ),
              );
            },
          ),
          SizedBox(
            height: 20,
          ),
          Text('$_attendanceSituation'),
          Center(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 55,
                ),
                if (_attendanceSituation == '')
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          primary: Colors.greenAccent,
                          //background color of button
                          side: BorderSide(width: 3, color: Colors.white),
                          //border width and color
                          elevation: 5,
                          //elevation of button
                          shape: RoundedRectangleBorder(
                              //to set border radius to button
                              borderRadius: BorderRadius.circular(20)),
                          padding:
                              EdgeInsets.all(20) //content padding inside button
                          ),
                      onPressed: () {
                        _setAttendance();
                      },
                      child: Text(
                        "출근",
                        style: TextStyle(fontSize: 50),
                      )),
                SizedBox( width:80,),
                if(_attendanceSituation == '출근')
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        primary: Colors.greenAccent,
                        //background color of button
                        side: BorderSide(width: 3, color: Colors.white),
                        //border width and color
                        elevation: 5,
                        //elevation of button
                        shape: RoundedRectangleBorder(
                            //to set border radius to button
                            borderRadius: BorderRadius.circular(20)),
                        padding:
                            EdgeInsets.all(20) //content padding inside button
                        ),
                    onPressed: () {
                      _putAttendance('EARLY_LEAVE');
                    },
                    child: Text(
                      "조퇴",
                      style: TextStyle(fontSize: 50),
                    )),
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          primary: Colors.greenAccent,
                          //background color of button
                          side: BorderSide(width: 3, color: Colors.white),
                          //border width and color
                          elevation: 5,
                          //elevation of button
                          shape: RoundedRectangleBorder(
                            //to set border radius to button
                              borderRadius: BorderRadius.circular(20)),
                          padding:
                          EdgeInsets.all(20) //content padding inside button
                      ),
                      onPressed: () {
                        _putAttendance('COMPANY_OUT');
                      },
                      child: Text(
                        "퇴근",
                        style: TextStyle(fontSize: 50),
                      )),
                if (_attendanceSituation == '퇴근' ||
                    _attendanceSituation == '조퇴')
                  Image.asset(
                    'assets/free.png',
                    width: 300,
                    height: 300,
                  ),
              ],
            ),
          ),
          SizedBox(
            height: 80,
          ),
          DataTable(
              decoration:
                  BoxDecoration(border: Border.all(color: Colors.black)),
              columns: [
                DataColumn(label: Text('출근 시간')),
                DataColumn(label: Text('조퇴 시간')),
                DataColumn(label: Text('퇴근 시간')),
              ],
              rows: [
                DataRow(cells: [
                  DataCell(Text(_timeCompanyIn)),
                  DataCell(Text(_timeEarlyLeave)),
                  DataCell(Text(_timeCompanyOut)),
                ])
              ]),
        ],
      ),
    );
  }
}
