import 'package:app_company_employee_manager/functions/token_lib.dart';
import 'package:app_company_employee_manager/pages/page_attendance_state.dart';
import 'package:app_company_employee_manager/pages/page_my_infomation.dart';
import 'package:app_company_employee_manager/pages/page_vacation_application.dart';
import 'package:app_company_employee_manager/pages/page_vacation_check.dart';
import 'package:flutter/material.dart';

class PageMyPage extends StatelessWidget {
  const PageMyPage ({Key? key}) : super(key: key);

  Future<void> _logout(BuildContext context) async {
    // functions/token_lib.dart 에서 만든 로그아웃 기능 호출
    TokenLib.logout(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('',
        ),
        centerTitle: false,
        elevation: 0.0,
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            UserAccountsDrawerHeader(
              currentAccountPicture: CircleAvatar(
                backgroundImage: AssetImage('assets/여자이미지6.png',),
                backgroundColor: Colors.white,
              ),
              accountName: Text('김새봄'),
              accountEmail: Text('ksb930204@naver.com'),
              onDetailsPressed: () {
                print('arrow is clicked');
              },
              decoration: BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(40.0),
                      bottomRight: Radius.circular(40.0)
                  )
              ),
            ),
            ListTile(
              leading: Icon(Icons.face_retouching_natural  ,
                  color: Colors.grey[900]
              ),
              title: Text('마이페이지'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder:(context) => const PageMyInfomation()));
              },
            ),

            ListTile(
              leading: Icon(Icons.business_center_rounded ,
                  color: Colors.grey[900]
              ),
              title: Text('출퇴근'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder:(context) => const PageAttendanceState()));
              },
            ),
            ListTile(
              leading: Icon(Icons.content_paste,
                  color: Colors.grey[900]),
              title: Text('연차신청'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder:(context) => const PageVacationApplication()));
              },
            ),
            ListTile(
              leading: Icon(Icons.content_paste_search_sharp,
                  color: Colors.grey[900]),
              title: Text('근태내역'),
              onTap: () {
              },
            ),
            ListTile(
              leading: Icon(Icons.sailing_outlined ,
                  color: Colors.grey[900]),
              title: Text('휴가조회'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder:(context) => const PageVacationCheck()));
              },
            ),
            ListTile(
              leading: Icon(Icons.settings_suggest,
                  color: Colors.grey[900]),
              title: Text('설정'),
              onTap: () {
                print('');
              },
            ),
          ],
        ),
      ),
    );
  }
}
