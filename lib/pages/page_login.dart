import 'package:app_company_employee_manager/components/component_custom_loading.dart';
import 'package:app_company_employee_manager/components/component_notification.dart';
import 'package:app_company_employee_manager/config/config_form_validator.dart';
import 'package:app_company_employee_manager/functions/token_lib.dart';
import 'package:app_company_employee_manager/middleware/middleware_login_check.dart';
import 'package:app_company_employee_manager/model/employee/employee_login_request.dart';
import 'package:app_company_employee_manager/repository/repo_employee.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PageLogin extends StatefulWidget {
  const PageLogin({Key? key}) : super(key: key);

  @override
  State<PageLogin> createState() => _PageLoginState();
}

class _PageLoginState extends State<PageLogin> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _doLogin(EmployeeLoginRequest loginRequest) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoEmployee().doLogin(loginRequest).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '로그인 성공',
        subTitle: '로그인에 성공하였습니다.',
      ).call();

      TokenLib.setToken(res.data.employeeId.toString());

      MiddleWareLoginCheck().check(context);
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '로그인 실패',
        subTitle: '아이디 혹은 비밀번호를 확인해주세요.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body:
      _buildBody(),
    );
  }

  Widget _buildBody() {
    return Material(
      child: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.cover,image: AssetImage('assets/배경이미지4.gif')),
            ),
        child: Column(
          children: [
            SizedBox(height: 50,),
            Column(
              children: [
                Image.asset(
                  'assets/로그인이미지3.png',
                  width: 250,
                  height: 250,
                ),
                SizedBox(height: 20,),
                FormBuilder(
                  key: _formKey,
                  autovalidateMode: AutovalidateMode.disabled,
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 10, right: 10, bottom: 10),
                        child: FormBuilderTextField(
                          name: 'username',
                          decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              prefixIcon: Icon(Icons.account_circle),
                              labelText: '아이디를 입력해주세요'),
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(
                                errorText: formErrorRequired),
                            FormBuilderValidators.minLength(5,
                                errorText: formErrorMinLength(5)),
                            FormBuilderValidators.maxLength(20,
                                errorText: formErrorMaxLength(20)),
                          ]),
                          keyboardType: TextInputType.text,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 10, right: 10),
                        child: FormBuilderTextField(
                          name: 'password',
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            prefixIcon: Icon(Icons.lock),
                            labelText: '비밀번호를 입력해주세요',
                          ),
                          obscureText: true,
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(
                                errorText: formErrorRequired),
                            FormBuilderValidators.minLength(8,
                                errorText: formErrorMinLength(8)),
                            FormBuilderValidators.maxLength(20,
                                errorText: formErrorMaxLength(20)),
                          ]),
                          keyboardType: TextInputType.text,
                        ),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Container(
                        height: 40,
                        width: 100,
                        child: OutlinedButton(
                          onPressed: () {
                            if (_formKey.currentState?.saveAndValidate() ?? false) {
                              EmployeeLoginRequest loginRequest =
                              EmployeeLoginRequest(
                                _formKey.currentState!.fields['username']!.value,
                                _formKey.currentState!.fields['password']!.value,
                              );
                              _doLogin(loginRequest);
                            }
                          },
                          child: Text(
                            '로그인',
                            style: TextStyle(color: Colors.black),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
