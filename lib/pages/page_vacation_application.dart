import 'package:app_company_employee_manager/components/component_appbar_popup.dart';
import 'package:app_company_employee_manager/components/component_custom_loading.dart';
import 'package:app_company_employee_manager/components/component_notification.dart';
import 'package:app_company_employee_manager/config/config_dropdown.dart';
import 'package:app_company_employee_manager/config/config_form_validator.dart';
import 'package:app_company_employee_manager/model/vacation/vacation_create_request.dart';
import 'package:app_company_employee_manager/repository/repo_vacation.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:intl/intl.dart';

class PageVacationApplication extends StatefulWidget {
  const PageVacationApplication({Key? key}) : super(key: key);

  @override
  State<PageVacationApplication> createState() => _PageVacationApplicationState();
}

class _PageVacationApplicationState extends State<PageVacationApplication> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _setVacation(VacationCreateRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoVacation().setData(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
          success: true,
          title: '휴가 등록 성공',
          subTitle: res.msg
      ).call();
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
          success: false,
          title: '휴가 등록 실패',
          subTitle: '휴가 등록에 실패하였습니다'
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(
        title: '휴가 신청'
      ),
      body: _buildBody(),

      );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: FormBuilder(
        key: _formKey,
        autovalidateMode: AutovalidateMode.disabled,
        child: Column(
          children: [
            SizedBox(height: 50,),
            Container(
              margin: EdgeInsets.all(10),
              alignment: Alignment.center,
              child: Column(
                children: [
                  Text('부서:관리부 \n직급:대리\n성명:김새봄',
                    style: TextStyle(fontSize: 14,),
                  ),
                ],
              ),
              decoration: BoxDecoration(
                  border: Border.all(
                      color: Colors.black38, width: 2.0,
                      style: BorderStyle.solid),
                  borderRadius: BorderRadius.circular(0),
                  color: Colors.white
              ),
            ),
            Container(
              margin: EdgeInsets.all(10),
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('근태유형',
                    style: TextStyle(fontSize: 14,),
                  ),
                  Icon(Icons.arrow_drop_down)
                ],
              ),
              decoration: BoxDecoration(
                  border: Border.all(
                      color: Colors.black38, width: 2.0,
                      style: BorderStyle.solid),
                  borderRadius: BorderRadius.circular(0),
                  color: Colors.white
              ),
            ),
            Container(
              margin: EdgeInsets.all(10),
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    '휴가일자 2022-09-01 ~ 2022-09-15',
                    style: TextStyle(
                      fontSize: 14,
                    ),
                  ),
                  Icon(Icons.calendar_month_rounded)
                ],
              ),
              decoration: BoxDecoration(
                  border: Border.all(
                      color: Colors.black38,
                      width: 2.0,
                      style: BorderStyle.solid),
                  borderRadius: BorderRadius.circular(0),
                  color: Colors.white),
            ),
            Container(
              margin: EdgeInsets.all(10),
              alignment: Alignment.center,
              child:Row(
                children: [
                  Text('시작시간\n 09시00분',
                    style: TextStyle(
                        fontWeight: FontWeight.w600
                    ),
                  ),
                ],
              ),
              decoration: BoxDecoration(
                  border: Border.all(
                      color: Colors.black38,
                      width: 2.0,
                      style: BorderStyle.solid),
                  borderRadius: BorderRadius.circular(0),
                  color: Colors.white),
            ),
            Container(
              margin: EdgeInsets.all(10),
              alignment: Alignment.center,
              child:Row(
                children: [
                  Text('종료시간\n 13시00분',
                    style: TextStyle(
                        fontWeight: FontWeight.w600
                    ),
                  ),
                ],
              ),
              decoration: BoxDecoration(
                  border: Border.all(
                      color: Colors.black38,
                      width: 2.0,
                      style: BorderStyle.solid),
                  borderRadius: BorderRadius.circular(0),
                  color: Colors.white),
            ),
              Container(
                margin: EdgeInsets.all(10),
                alignment: Alignment.center,
                child:Row(
                  children: [
                    Text('신청사유\n\n병원진료',
                      style: TextStyle(
                      ),
                    ),
                  ],
                ),
                decoration: BoxDecoration(
                    border: Border.all(
                        color: Colors.black38,
                        width: 2.0,
                        style: BorderStyle.solid),
                    borderRadius: BorderRadius.circular(0),
                    color: Colors.white),
              ),
            OutlinedButton(
                onPressed: () {
                  if (_formKey.currentState?.saveAndValidate()??false) {
                    VacationCreateRequest request = VacationCreateRequest(
                      _formKey.currentState!.fields['vacationName']!.value,
                      _formKey.currentState!.fields['vacationType']!.value,
                      DateFormat('yyyy-MM-dd').format(_formKey.currentState!.fields['dateVacation']!.value),
                      _formKey.currentState!.fields['vacationReason']!.value,
                    );

                    _setVacation(request);
                  }
                },
                child: Text('신청하기')
            ),
          ],
        )
      ),
    );
  }
}
