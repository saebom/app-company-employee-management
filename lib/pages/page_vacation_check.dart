import 'package:app_company_employee_manager/components/component_appbar_popup.dart';
import 'package:app_company_employee_manager/components/component_custom_loading.dart';
import 'package:app_company_employee_manager/components/component_notification.dart';
import 'package:app_company_employee_manager/config/config_color.dart';
import 'package:app_company_employee_manager/config/config_decoration.dart';
import 'package:app_company_employee_manager/config/config_form_decoration.dart';
import 'package:app_company_employee_manager/config/config_form_validator.dart';
import 'package:app_company_employee_manager/config/config_size.dart';
import 'package:app_company_employee_manager/model/vacation/vacation_create_request.dart';
import 'package:app_company_employee_manager/model/vacation/vacation_list_item.dart';
import 'package:app_company_employee_manager/pages/page_vacation_application.dart';
import 'package:app_company_employee_manager/repository/repo_vacation.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_neat_and_clean_calendar/flutter_neat_and_clean_calendar.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:intl/intl.dart';

class PageVacationCheck extends StatefulWidget {
  const PageVacationCheck({Key? key}) : super(key: key);

  @override
  State<PageVacationCheck> createState() => _PageVacationCheckState();
}

class _PageVacationCheckState extends State<PageVacationCheck> {
  final GlobalKey<FormBuilderState> _formKey = GlobalKey<FormBuilderState>();

  Map<DateTime, List<NeatCleanCalendarEvent>> _events = {};

  late DateTime _selectedDay;
  late DateTime _today;

  bool _isViewDetail = false;

  Map<DateTime, List<NeatCleanCalendarEvent>> _convertDayEvents(
      List<VacationListItem>? items) {
    Map<DateTime, List<NeatCleanCalendarEvent>> result = {};

    for (VacationListItem item in items!) {
      List<NeatCleanCalendarEvent> inList = [];
      inList.add(NeatCleanCalendarEvent(item.vacationType,
          description: item.vacationReason,
          isAllDay: true,
          startTime: item.dateVacation,
          endTime: item.dateVacation));

      result.putIfAbsent(item.dateVacation, () => inList);
    }

    return result;
  }

  Future<void> _loadItems() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoVacation()
        .getList()
        .then((res) => {
      BotToast.closeAllLoading(),
      setState(() {
        _events = _convertDayEvents(res.list);
      })
    })
        .catchError((err) => {
      BotToast.closeAllLoading(),
    });
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      _today = DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);
      _selectedDay = DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);
    });
    _loadItems();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: ComponentAppbarPopup(
          title: '휴가 조회',
        ),
        body: SafeArea(
          child: Calendar(
            onDateSelected: _handleSelectedDate,
            onMonthChanged: _handleMonthChange,
            eventListBuilder: _eventListBuilder,
            hideTodayIcon: true,
            startOnMonday: false,
            weekDays: ['일', '월', '화', '수', '목', '금', '토'],
            events: _events,
            isExpandable: false,
            isExpanded: true,
            eventDoneColor: Colors.green,
            selectedColor: Colors.pink,
            todayColor: Colors.blue,
            eventColor: Colors.grey,
            locale: 'ko_KR',
            expandableDateFormat: 'yyyy년 MM월 dd일 EEEE',
            dayOfWeekStyle: TextStyle(
                color: Colors.black, fontWeight: FontWeight.w800, fontSize: 11),
          ),
        )
    );
  }

  Widget _eventListBuilder(BuildContext context, List<NeatCleanCalendarEvent> eventList) {
    if (_isViewDetail) {
      if (eventList.length <= 0) {
        if (_selectedDay.isAfter(_today) || _selectedDay.isAtSameMomentAs(_today)) {
          return Expanded(
            child: ListView(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('휴가 일정', style: TextStyle(fontSize:20, fontWeight: FontWeight.bold),),
                    Container(
                      child: IconButton(
                        icon: Icon(Icons.playlist_add),
                        iconSize: 30,
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => PageVacationApplication())
                          );
                        },
                      ),
                    ),
                  ],
                )
              ],
            ),
          );
        } else {
          return Container(
            alignment: Alignment.centerLeft,
            child: Text('휴가 일정', style: TextStyle(fontSize:20, fontWeight: FontWeight.bold,)),
          );
        }
      } else {
        return Expanded(
          child: ListView.builder(
            padding: EdgeInsets.all(0.0),
            itemBuilder: (BuildContext context, int index) {
              final NeatCleanCalendarEvent event = eventList[index];
              return ListTile(
                contentPadding:
                EdgeInsets.only(left: 2.0, right: 8.0, top: 2.0, bottom: 2.0),
                leading: Container(
                  width: 10.0,
                  color: event.color,
                ),
                title: Text(
                  event.summary,
                  style: TextStyle(height: 1.5, fontSize: fontSizeMid),
                ),
                subtitle: Text(
                  event.description,
                  style: TextStyle(
                      height: 1.3, color: colorGray, fontSize: fontSizeSm),
                ),
                trailing: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [isShowDelButton(event.startTime)],
                ),
              );
            },
            itemCount: eventList.length,
          ),
        );
      }
    } else {
      return Container();
    }

  }

  Widget isShowDelButton(DateTime dateHoliday) {
    String formattedDate = DateFormat('yyyy-MM-dd').format(dateHoliday);

    if (dateHoliday.isAfter(_today)) {
      return IconButton(
        icon: Icon(Icons.dangerous),
        color: colorPrimary,
        onPressed: () {
          _showDelDialog(formattedDate);
        },
      );
    } else {
      return IconButton(
        icon: Icon(Icons.dangerous),
        color: colorLightGray,
        onPressed: () {},
      );
    }
  }

  Future<void> _doDelHoliday(BuildContext context, String dateHoliday) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoVacation().delData(dateHoliday).then((res) {
      BotToast.closeAllLoading();
      _loadItems();
      ComponentNotification(
        success: true,
        title: '휴무 취소 성공',
        subTitle: '휴무 취소가 성공하였습니다.',
      ).call();
      setState(() {
        _isViewDetail = false;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();
      ComponentNotification(
        success: false,
        title: '휴무 취소 실패',
        subTitle: '관리자에게 문의하세요.',
      ).call();
    });
  }

  void _showDelDialog(String dateHoliday) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('$dateHoliday 휴무 취소'),
            content: Text('정말 $dateHoliday 휴무를 취소하시겠습니까?'),
            actions: [
              OutlinedButton(
                child: const Text('확인'),
                onPressed: () async {
                  _doDelHoliday(context, dateHoliday);
                  Navigator.of(context).pop();
                },
              ),
              OutlinedButton(
                child: const Text('취소'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
  }

  void _handleMonthChange(date) {
    setState(() {
      _isViewDetail = true;
    });
  }

  void _handleSelectedDate(date) {
    setState(() {
      _selectedDay = date;
    });
  }
}
