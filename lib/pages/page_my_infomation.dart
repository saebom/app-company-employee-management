import 'package:app_company_employee_manager/components/component_custom_loading.dart';
import 'package:app_company_employee_manager/components/component_notification.dart';
import 'package:app_company_employee_manager/model/employee/employee_detail.dart';
import 'package:app_company_employee_manager/repository/repo_employee.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';

class PageMyInfomation extends StatefulWidget {
  const PageMyInfomation({Key? key}) : super(key: key);

  @override
  State<PageMyInfomation> createState() => _PageMypagetwoState();
}

class _PageMypagetwoState extends State<PageMyInfomation> {
  EmployeeDetail? _employeeDetail;

  void initState() {
    super.initState();
    _getMyInfo();
  }
  Future<void> _getMyInfo() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc){
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoEmployee().getEmployeeDetail().then((res) {
      BotToast.closeAllLoading();



      ComponentNotification(
          success: true,
          title: '마이페이지 업로드 성공',
          subTitle: res.msg
      );
      setState(() {
        _employeeDetail = res.data;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
          success: false,
          title: '마이페이지 업로드 실패',
          subTitle: "마이페이지 업로드에 실패하였습니다."
      );
    });
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('마이페이지'),
          centerTitle: false,
          elevation: 0.0,
        ),
        body:SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: 20,),
              Container(
                alignment: Alignment.center,
                height: 200,
                child: Image.asset(
                  'assets/여자이미지6.png',
                  width: 170,
                ),
              ),
              SizedBox(height: 20,),
              Container(
                height: 240,
                width: 350,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    color: Colors.white38,
                    border: Border.all(
                        color: Colors.black26,
                        style: BorderStyle.solid,
                        width: 10)),
                child:Column(
                  children: [
                    SizedBox(height: 10,),
                    Row( children: [
                      SizedBox(width: 10,height: 20,),
                      Text('이름:${_employeeDetail?.employeeName ?? '-'}',
                        style: TextStyle(
                            fontSize: 14
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ],
                    ),
                    Divider(color: Colors.grey,),
                    Row( children: [
                      SizedBox(width: 10,height: 20,),
                      Text('생년월일:${_employeeDetail?.employeeBirthday ?? '-'}',
                        style: TextStyle(
                            fontSize: 14
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ],
                    ),
                    Divider(color: Colors.grey,),
                    Row( children: [
                      SizedBox(width: 10,height: 20,),
                      Text('아이디:${_employeeDetail?.username ?? '-'}',
                        style: TextStyle(
                            fontSize: 14
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ],
                    ),
                    Divider(color: Colors.grey,),
                    Row( children: [
                      SizedBox(width: 10,height: 20,),
                      Text('부서:${_employeeDetail?.employeeField ?? '-'}',
                        style: TextStyle(
                            fontSize: 14
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ],
                    ),
                    Divider(color: Colors.grey,),
                    Row( children: [
                      SizedBox(width: 10,height: 20,),
                      Text('직급:${_employeeDetail?.employeeGrade?? '-'}',
                        style: TextStyle(
                            fontSize: 14
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ],
                    ),
                    Divider(color: Colors.grey,),
                    Row( children: [
                      SizedBox(width: 10,height: 20,),
                      Text('입사일:${_employeeDetail?.dateIn ?? '-'}',
                        style: TextStyle(
                            fontSize: 14
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        )
    );
  }
}
