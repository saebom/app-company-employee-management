import 'package:app_company_employee_manager/functions/token_lib.dart';
import 'package:app_company_employee_manager/pages/page_login.dart';
import 'package:app_company_employee_manager/pages/page_my_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MiddleWareLoginCheck {
  void check(BuildContext context) async {
    String? token = await TokenLib.getToken();

    if (token == null || token == '') {
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => PageLogin()), (route) => false);
    } else {
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => PageMyPage()), (route) => false);
    }

  }
}