import 'package:flutter/material.dart';

class ComponentListItem extends StatelessWidget {
  const ComponentListItem({super.key, required this.menuListName, required this.callback});
  final String menuListName;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child:Center(
        child: Container(
          width: 360,
          height: 50,
          margin: EdgeInsets.all(20),
          decoration: BoxDecoration(
              color: Colors.blue
          ),
          child: Text(menuListName, style: TextStyle(color: Colors.white, letterSpacing: 5, height: 2),textAlign: TextAlign.center, ),
        ),
      ),
      onTap: callback,
    );
  }
}
