
import 'package:app_company_employee_manager/model/vacation/vacation_list_item.dart';
import 'package:flutter/material.dart';

class ComponentAttendanceList extends StatelessWidget {
  const ComponentAttendanceList({super.key, required this.item});
  final VacationListItem item;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.only(right:10, left: 10, bottom: 10),
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: const Color.fromRGBO(100, 100, 100, 100))
      ),
      child: Row(
        children: [
          Text(item.vacationName),
          Text(item.vacationType),
          Text(item.dateVacation.toString()),
          Text(item.vacationReason),
        ],
      )
    );
  }
}
