import 'package:app_company_employee_manager/config/config_api.dart';
import 'package:app_company_employee_manager/functions/token_lib.dart';
import 'package:app_company_employee_manager/model/attendance/attendance_situation_single_result.dart';
import 'package:app_company_employee_manager/model/common_result.dart';
import 'package:dio/dio.dart';

class RepoAttendance {
  Future<CommonResult> setAttendance() async {
    String baserUrl = '$apiUri/attendance/employee/company-in/{employeeId}';

    Dio dio = Dio();

    String? token  = await TokenLib.getToken();

    final response = await dio.post(
      baserUrl.replaceAll('{employeeId}', token!),
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )
    );

    return CommonResult.fromJson(response.data);
  }

  Future<AttendanceSituationSingleResult> getMySituation() async {
    String baseUrl = '$apiUri/attendance/my/attendance-situation/{employeeId}';

    Dio dio = Dio();

    String? token = await TokenLib.getToken();

    final response = await dio.get(
      baseUrl.replaceAll('{employeeId}', token!),
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )
    );

    return AttendanceSituationSingleResult.fromJson(response.data);
  }

  Future<CommonResult> putAttendanceSituation(String attendanceSituation) async {
    String baseUrl = '$apiUri/attendance/employee/{employeeId}/attendance-situation-change/{attendanceSituation}';
    
    Dio dio = Dio();
    
    String? token = await TokenLib.getToken();
    
    final response = await dio.put(
      baseUrl.replaceAll('{employeeId}', token!).replaceAll('{attendanceSituation}', attendanceSituation),
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )
    );


    return CommonResult.fromJson(response.data);
  }
}