import 'package:flutter/material.dart';

const List<DropdownMenuItem<String>> dropdownVacationType = [
  DropdownMenuItem(value: 'ANNUAL', child: Text('연차')),
  DropdownMenuItem(value: 'HALFWAY', child: Text('반차')),
];